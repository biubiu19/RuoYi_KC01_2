package com.ruoyi.quartz.task;

import com.ruoyi.system.domain.Mistakes;
import com.ruoyi.system.domain.Tyqc;
import com.ruoyi.system.service.IMistakesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("gdsTask")
public class GdsTask {
    @Autowired
    private IMistakesService iMistakesService;
    public void gyprintEqui(){
        List<Mistakes> mistakes = iMistakesService.selectMistakesList(new Mistakes());
        System.out.println(mistakes.toString());
    }
}
