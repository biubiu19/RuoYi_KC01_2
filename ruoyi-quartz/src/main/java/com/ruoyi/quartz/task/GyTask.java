package com.ruoyi.quartz.task;

import com.ruoyi.system.domain.Tyqc;
import com.ruoyi.system.service.ITyqcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("gyTask")
public class GyTask {

    @Autowired
    private ITyqcService iTyqcService;
    public void printEqui(){
        List<Tyqc> tyqcs = iTyqcService.selectTyqcList(new Tyqc());
        System.out.println(tyqcs.toString());
    }

    public void printEquiById(Long id){
        System.out.println("id = " + id);
        Tyqc tyqc = iTyqcService.selectTyqcById(id);
        System.out.println(tyqc.toString());
    }
}
