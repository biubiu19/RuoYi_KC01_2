package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Cwm;
import com.ruoyi.system.service.ICwmService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 宠物猫信息Controller
 * 
 * @author ruoyi
 * @date 2024-02-21
 */
@Controller
@RequestMapping("/system/cwm")
public class CwmController extends BaseController
{
    private String prefix = "system/cwm";

    @Autowired
    private ICwmService cwmService;

    @RequiresPermissions("system:cwm:view")
    @GetMapping()
    public String cwm()
    {
        return prefix + "/cwm";
    }

    /**
     * 查询宠物猫信息列表
     */
    @RequiresPermissions("system:cwm:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Cwm cwm)
    {
        startPage();
        List<Cwm> list = cwmService.selectCwmList(cwm);
        return getDataTable(list);
    }

    /**
     * 导出宠物猫信息列表
     */
    @RequiresPermissions("system:cwm:export")
    @Log(title = "宠物猫信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Cwm cwm)
    {
        List<Cwm> list = cwmService.selectCwmList(cwm);
        ExcelUtil<Cwm> util = new ExcelUtil<Cwm>(Cwm.class);
        return util.exportExcel(list, "宠物猫信息数据");
    }

    /**
     * 新增宠物猫信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存宠物猫信息
     */
    @RequiresPermissions("system:cwm:add")
    @Log(title = "宠物猫信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Cwm cwm)
    {
        return toAjax(cwmService.insertCwm(cwm));
    }

    /**
     * 修改宠物猫信息
     */
    @RequiresPermissions("system:cwm:edit")
    @GetMapping("/edit/{dogId}")
    public String edit(@PathVariable("dogId") Long dogId, ModelMap mmap)
    {
        Cwm cwm = cwmService.selectCwmByDogId(dogId);
        mmap.put("cwm", cwm);
        return prefix + "/edit";
    }

    /**
     * 修改保存宠物猫信息
     */
    @RequiresPermissions("system:cwm:edit")
    @Log(title = "宠物猫信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Cwm cwm)
    {
        return toAjax(cwmService.updateCwm(cwm));
    }

    /**
     * 删除宠物猫信息
     */
    @RequiresPermissions("system:cwm:remove")
    @Log(title = "宠物猫信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(cwmService.deleteCwmByDogIds(ids));
    }
}
