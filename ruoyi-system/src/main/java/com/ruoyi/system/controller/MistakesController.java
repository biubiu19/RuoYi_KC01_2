package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Mistakes;
import com.ruoyi.system.service.IMistakesService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 错题管理Controller
 * 
 * @author ruoyi
 * @date 2024-03-11
 */
@Controller
@RequestMapping("/system/mistakes")
public class MistakesController extends BaseController
{
    private String prefix = "system/mistakes";

    @Autowired
    private IMistakesService mistakesService;

    @RequiresPermissions("system:mistakes:view")
    @GetMapping()
    public String mistakes()
    {
        return prefix + "/mistakes";
    }

    /**
     * 查询错题管理列表
     */
    @RequiresPermissions("system:mistakes:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Mistakes mistakes)
    {
        startPage();
        List<Mistakes> list = mistakesService.selectMistakesList(mistakes);
        return getDataTable(list);
    }

    /**
     * 导出错题管理列表
     */
    @RequiresPermissions("system:mistakes:export")
    @Log(title = "错题管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Mistakes mistakes)
    {
        List<Mistakes> list = mistakesService.selectMistakesList(mistakes);
        ExcelUtil<Mistakes> util = new ExcelUtil<Mistakes>(Mistakes.class);
        return util.exportExcel(list, "错题管理数据");
    }

    /**
     * 新增错题管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存错题管理
     */
    @RequiresPermissions("system:mistakes:add")
    @Log(title = "错题管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Mistakes mistakes)
    {
        return toAjax(mistakesService.insertMistakes(mistakes));
    }

    /**
     * 修改错题管理
     */
    @RequiresPermissions("system:mistakes:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Mistakes mistakes = mistakesService.selectMistakesById(id);
        mmap.put("mistakes", mistakes);
        return prefix + "/edit";
    }

    /**
     * 修改保存错题管理
     */
    @RequiresPermissions("system:mistakes:edit")
    @Log(title = "错题管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Mistakes mistakes)
    {
        return toAjax(mistakesService.updateMistakes(mistakes));
    }

    /**
     * 删除错题管理
     */
    @RequiresPermissions("system:mistakes:remove")
    @Log(title = "错题管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mistakesService.deleteMistakesByIds(ids));
    }
}
