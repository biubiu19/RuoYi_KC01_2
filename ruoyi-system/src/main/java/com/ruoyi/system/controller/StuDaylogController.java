package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.StuDaylog;
import com.ruoyi.system.service.IStuDaylogService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 日志Controller
 * 
 * @author ruoyi
 * @date 2024-02-05
 */
@Controller
@RequestMapping("/system/daylog")
public class StuDaylogController extends BaseController
{
    private String prefix = "system/daylog";

    @Autowired
    private IStuDaylogService stuDaylogService;

    @RequiresPermissions("system:daylog:view")
    @GetMapping()
    public String daylog()
    {
        return prefix + "/daylog";
    }

    /**
     * 查询日志列表
     */
    @RequiresPermissions("system:daylog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StuDaylog stuDaylog)
    {
        startPage();
        List<StuDaylog> list = stuDaylogService.selectStuDaylogList(stuDaylog);
        return getDataTable(list);
    }

    /**
     * 导出日志列表
     */
    @RequiresPermissions("system:daylog:export")
    @Log(title = "日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StuDaylog stuDaylog)
    {
        List<StuDaylog> list = stuDaylogService.selectStuDaylogList(stuDaylog);
        ExcelUtil<StuDaylog> util = new ExcelUtil<StuDaylog>(StuDaylog.class);
        return util.exportExcel(list, "日志数据");
    }

    /**
     * 新增日志
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存日志
     */
    @RequiresPermissions("system:daylog:add")
    @Log(title = "日志", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StuDaylog stuDaylog)
    {
        return toAjax(stuDaylogService.insertStuDaylog(stuDaylog));
    }

    /**
     * 修改日志
     */
    @RequiresPermissions("system:daylog:edit")
    @GetMapping("/edit/{logId}")
    public String edit(@PathVariable("logId") Long logId, ModelMap mmap)
    {
        StuDaylog stuDaylog = stuDaylogService.selectStuDaylogByLogId(logId);
        mmap.put("stuDaylog", stuDaylog);
        return prefix + "/edit";
    }

    /**
     * 修改保存日志
     */
    @RequiresPermissions("system:daylog:edit")
    @Log(title = "日志", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StuDaylog stuDaylog)
    {
        return toAjax(stuDaylogService.updateStuDaylog(stuDaylog));
    }

    /**
     * 删除日志
     */
    @RequiresPermissions("system:daylog:remove")
    @Log(title = "日志", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(stuDaylogService.deleteStuDaylogByLogIds(ids));
    }
}
