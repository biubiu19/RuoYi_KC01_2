package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Tyqc;
import com.ruoyi.system.service.ITyqcService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 器材管理Controller
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
@Controller
@RequestMapping("/system/tyqc")
public class TyqcController extends BaseController
{
    private String prefix = "system/tyqc";

    @Autowired
    private ITyqcService iTyqcService;

    @RequiresPermissions("system:tyqc:view")
    @GetMapping()
    public String tyqc()
    {
        return prefix + "/tyqc";
    }

    /**
     * 查询器材管理列表
     */
    @RequiresPermissions("system:tyqc:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Tyqc tyqc)
    {
        startPage();
        List<Tyqc> list = iTyqcService.selectTyqcList(tyqc);
        return getDataTable(list);
    }

    /**
     * 导出器材管理列表
     */
    @RequiresPermissions("system:tyqc:export")
    @Log(title = "器材管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Tyqc tyqc)
    {
        List<Tyqc> list = iTyqcService.selectTyqcList(tyqc);
        ExcelUtil<Tyqc> util = new ExcelUtil<Tyqc>(Tyqc.class);
        return util.exportExcel(list, "器材管理数据");
    }

    /**
     * 新增器材管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存器材管理
     */
    @RequiresPermissions("system:tyqc:add")
    @Log(title = "器材管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Tyqc tyqc)
    {
        return toAjax(iTyqcService.insertTyqc(tyqc));
    }

    /**
     * 修改器材管理
     */
    @RequiresPermissions("system:tyqc:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Tyqc tyqc = iTyqcService.selectTyqcById(id);
        mmap.put("tyqc", tyqc);
        return prefix + "/edit";
    }

    /**
     * 修改保存器材管理
     */
    @RequiresPermissions("system:tyqc:edit")
    @Log(title = "器材管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Tyqc tyqc)
    {
        return toAjax(iTyqcService.updateTyqc(tyqc));
    }

    /**
     * 删除器材管理
     */
    @RequiresPermissions("system:tyqc:remove")
    @Log(title = "器材管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(iTyqcService.deleteTyqcByIds(ids));
    }

    /**
     * 该器材数量+1
     */
    @RequiresPermissions("system:equi:addone")
    @Log(title = "器材管理", businessType = BusinessType.UPDATE)
    @GetMapping( "/addOne/{id}")
    @ResponseBody
    public AjaxResult addOne(@PathVariable("id") Integer id)
    {
        int count = iTyqcService.addOne(id);
        String massage = "";
        if (count == 1){
            massage = "添加成功";
        }else {
            massage = "添加失败";
        }

        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("massage", massage);
        System.out.println("massage = " + massage);
        return ajaxResult;
    }
}

