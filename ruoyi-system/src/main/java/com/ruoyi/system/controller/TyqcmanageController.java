package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Tyqcmanage;
import com.ruoyi.system.service.ITyqcmanageService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 管理器材老师Controller
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
@Controller
@RequestMapping("/system/tyqcmanage")
public class TyqcmanageController extends BaseController
{
    private String prefix = "system/tyqcmanage";

    @Autowired
    private ITyqcmanageService tyqcmanageService;

    @RequiresPermissions("system:tyqcmanage:view")
    @GetMapping()
    public String tyqcmanage()
    {
        return prefix + "/tyqcmanage";
    }

    /**
     * 查询管理器材老师列表
     */
    @RequiresPermissions("system:tyqcmanage:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Tyqcmanage tyqcmanage)
    {
        startPage();
        List<Tyqcmanage> list = tyqcmanageService.selectTyqcmanageList(tyqcmanage);
        return getDataTable(list);
    }

    /**
     * 导出管理器材老师列表
     */
    @RequiresPermissions("system:tyqcmanage:export")
    @Log(title = "管理器材老师", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Tyqcmanage tyqcmanage)
    {
        List<Tyqcmanage> list = tyqcmanageService.selectTyqcmanageList(tyqcmanage);
        ExcelUtil<Tyqcmanage> util = new ExcelUtil<Tyqcmanage>(Tyqcmanage.class);
        return util.exportExcel(list, "管理器材老师数据");
    }

    /**
     * 新增管理器材老师
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存管理器材老师
     */
    @RequiresPermissions("system:tyqcmanage:add")
    @Log(title = "管理器材老师", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Tyqcmanage tyqcmanage)
    {
        return toAjax(tyqcmanageService.insertTyqcmanage(tyqcmanage));
    }

    /**
     * 修改管理器材老师
     */
    @RequiresPermissions("system:tyqcmanage:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Tyqcmanage tyqcmanage = tyqcmanageService.selectTyqcmanageById(id);
        mmap.put("tyqcmanage", tyqcmanage);
        return prefix + "/edit";
    }

    /**
     * 修改保存管理器材老师
     */
    @RequiresPermissions("system:tyqcmanage:edit")
    @Log(title = "管理器材老师", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Tyqcmanage tyqcmanage)
    {
        return toAjax(tyqcmanageService.updateTyqcmanage(tyqcmanage));
    }

    /**
     * 删除管理器材老师
     */
    @RequiresPermissions("system:tyqcmanage:remove")
    @Log(title = "管理器材老师", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tyqcmanageService.deleteTyqcmanageByIds(ids));
    }
}
