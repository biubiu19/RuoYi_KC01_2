package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 宠物猫信息对象 cwm
 * 
 * @author ruoyi
 * @date 2024-02-21
 */
public class Cwm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键自增 */
    private Long dogId;

    /** 宠物猫Id */
    @Excel(name = "宠物猫Id")
    private Long cwmId;

    /** 宠物猫名字 */
    @Excel(name = "宠物猫名字")
    private String cwmName;

    /** 身体状况 */
    @Excel(name = "身体状况")
    private String cwmContent;

    public void setDogId(Long dogId) 
    {
        this.dogId = dogId;
    }

    public Long getDogId() 
    {
        return dogId;
    }
    public void setCwmId(Long cwmId) 
    {
        this.cwmId = cwmId;
    }

    public Long getCwmId() 
    {
        return cwmId;
    }
    public void setCwmName(String cwmName) 
    {
        this.cwmName = cwmName;
    }

    public String getCwmName() 
    {
        return cwmName;
    }
    public void setCwmContent(String cwmContent) 
    {
        this.cwmContent = cwmContent;
    }

    public String getCwmContent() 
    {
        return cwmContent;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dogId", getDogId())
            .append("cwmId", getCwmId())
            .append("cwmName", getCwmName())
            .append("cwmContent", getCwmContent())
            .toString();
    }
}
