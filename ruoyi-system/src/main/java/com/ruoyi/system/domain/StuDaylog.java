package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 日志对象 stu_daylog
 *
 * @author ruoyi
 * @date 2024-02-07
 */
public class StuDaylog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键自增 */
    private Long logId;

    /** 班级id */
    @Excel(name = "班级id")
    private Long classId;

    /** 班级名字 */
    @Excel(name = "班级名字")
    private String className;

    /** 学生id */
    @Excel(name = "学生id")
    private Long studentId;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String studentName;

    /** 日志内容 */
    @Excel(name = "日志内容")
    private String logContent;

    /** 课程评价 */
    @Excel(name = "课程评价")
    private String evaluate;

    public void setLogId(Long logId)
    {
        this.logId = logId;
    }

    public Long getLogId()
    {
        return logId;
    }
    public void setClassId(Long classId)
    {
        this.classId = classId;
    }

    public Long getClassId()
    {
        return classId;
    }
    public void setClassName(String className)
    {
        this.className = className;
    }

    public String getClassName()
    {
        return className;
    }
    public void setStudentId(Long studentId)
    {
        this.studentId = studentId;
    }

    public Long getStudentId()
    {
        return studentId;
    }
    public void setStudentName(String studentName)
    {
        this.studentName = studentName;
    }

    public String getStudentName()
    {
        return studentName;
    }
    public void setLogContent(String logContent)
    {
        this.logContent = logContent;
    }

    public String getLogContent()
    {
        return logContent;
    }
    public void setEvaluate(String evaluate)
    {
        this.evaluate = evaluate;
    }

    public String getEvaluate()
    {
        return evaluate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("logId", getLogId())
                .append("classId", getClassId())
                .append("className", getClassName())
                .append("studentId", getStudentId())
                .append("studentName", getStudentName())
                .append("logContent", getLogContent())
                .append("evaluate", getEvaluate())
                .toString();
    }
}
