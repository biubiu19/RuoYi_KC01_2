package com.ruoyi.system.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 器材管理对象 tyqc
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
public class Tyqc extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 逐渐自增 */
    private Long id;

    /** 器材名字 */
    @Excel(name = "器材名字")
    private String name;

    /** 器材数量 */
    @Excel(name = "器材数量")
    private Long quantiyi;

    /** 器材单价 */
    @Excel(name = "器材单价")
    private Long price;

    /** 能否出借 */
    @Excel(name = "能否出借")
    private Long lend;

    /** 管理器材老师信息 */
    private List<Tyqcmanage> tyqcmanageList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setQuantiyi(Long quantiyi) 
    {
        this.quantiyi = quantiyi;
    }

    public Long getQuantiyi() 
    {
        return quantiyi;
    }
    public void setPrice(Long price) 
    {
        this.price = price;
    }

    public Long getPrice() 
    {
        return price;
    }
    public void setLend(Long lend) 
    {
        this.lend = lend;
    }

    public Long getLend() 
    {
        return lend;
    }

    public List<Tyqcmanage> getTyqcmanageList()
    {
        return tyqcmanageList;
    }

    public void setTyqcmanageList(List<Tyqcmanage> tyqcmanageList)
    {
        this.tyqcmanageList = tyqcmanageList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("quantiyi", getQuantiyi())
            .append("price", getPrice())
            .append("lend", getLend())
            .append("tyqcmanageList", getTyqcmanageList())
            .toString();
    }
}
