package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 管理器材老师对象 tyqcg
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
public class Tyqcg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键自增 */
    private Long id;

    /** 器材id */
    @Excel(name = "器材id")
    private Long equiId;

    /** 器材管理者名字 */
    @Excel(name = "器材管理者名字")
    private String manName;

    /** 器材管理者年龄 */
    @Excel(name = "器材管理者年龄")
    private Long manAge;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEquiId(Long equiId) 
    {
        this.equiId = equiId;
    }

    public Long getEquiId() 
    {
        return equiId;
    }
    public void setManName(String manName) 
    {
        this.manName = manName;
    }

    public String getManName() 
    {
        return manName;
    }
    public void setManAge(Long manAge) 
    {
        this.manAge = manAge;
    }

    public Long getManAge() 
    {
        return manAge;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("equiId", getEquiId())
            .append("manName", getManName())
            .append("manAge", getManAge())
            .toString();
    }
}
