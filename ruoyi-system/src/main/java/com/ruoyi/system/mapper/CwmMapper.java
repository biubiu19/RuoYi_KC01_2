package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Cwm;

/**
 * 宠物猫信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-02-21
 */
public interface CwmMapper 
{
    /**
     * 查询宠物猫信息
     * 
     * @param dogId 宠物猫信息主键
     * @return 宠物猫信息
     */
    public Cwm selectCwmByDogId(Long dogId);

    /**
     * 查询宠物猫信息列表
     * 
     * @param cwm 宠物猫信息
     * @return 宠物猫信息集合
     */
    public List<Cwm> selectCwmList(Cwm cwm);

    /**
     * 新增宠物猫信息
     * 
     * @param cwm 宠物猫信息
     * @return 结果
     */
    public int insertCwm(Cwm cwm);

    /**
     * 修改宠物猫信息
     * 
     * @param cwm 宠物猫信息
     * @return 结果
     */
    public int updateCwm(Cwm cwm);

    /**
     * 删除宠物猫信息
     * 
     * @param dogId 宠物猫信息主键
     * @return 结果
     */
    public int deleteCwmByDogId(Long dogId);

    /**
     * 批量删除宠物猫信息
     * 
     * @param dogIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCwmByDogIds(String[] dogIds);
}
