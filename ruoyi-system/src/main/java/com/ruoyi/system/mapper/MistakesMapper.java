package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Mistakes;

/**
 * 错题管理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-03-11
 */
public interface MistakesMapper 
{
    /**
     * 查询错题管理
     * 
     * @param id 错题管理主键
     * @return 错题管理
     */
    public Mistakes selectMistakesById(Long id);

    /**
     * 查询错题管理列表
     * 
     * @param mistakes 错题管理
     * @return 错题管理集合
     */
    public List<Mistakes> selectMistakesList(Mistakes mistakes);

    /**
     * 新增错题管理
     * 
     * @param mistakes 错题管理
     * @return 结果
     */
    public int insertMistakes(Mistakes mistakes);

    /**
     * 修改错题管理
     * 
     * @param mistakes 错题管理
     * @return 结果
     */
    public int updateMistakes(Mistakes mistakes);

    /**
     * 删除错题管理
     * 
     * @param id 错题管理主键
     * @return 结果
     */
    public int deleteMistakesById(Long id);

    /**
     * 批量删除错题管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMistakesByIds(String[] ids);
}
