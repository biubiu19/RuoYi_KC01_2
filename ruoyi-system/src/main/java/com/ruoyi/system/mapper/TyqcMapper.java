package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Tyqc;
import com.ruoyi.system.domain.Tyqcmanage;

/**
 * 器材管理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
public interface TyqcMapper 
{
    /**
     * 查询器材管理
     * 
     * @param id 器材管理主键
     * @return 器材管理
     */
    public Tyqc selectTyqcById(Long id);

    /**
     * 查询器材管理列表
     * 
     * @param tyqc 器材管理
     * @return 器材管理集合
     */
    public List<Tyqc> selectTyqcList(Tyqc tyqc);

    /**
     * 新增器材管理
     * 
     * @param tyqc 器材管理
     * @return 结果
     */
    public int insertTyqc(Tyqc tyqc);

    /**
     * 修改器材管理
     * 
     * @param tyqc 器材管理
     * @return 结果
     */
    public int updateTyqc(Tyqc tyqc);

    /**
     * 删除器材管理
     * 
     * @param id 器材管理主键
     * @return 结果
     */
    public int deleteTyqcById(Long id);

    /**
     * 批量删除器材管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTyqcByIds(String[] ids);

    /**
     * 批量删除管理器材老师
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTyqcmanageByIds(String[] ids);
    
    /**
     * 批量新增管理器材老师
     * 
     * @param tyqcmanageList 管理器材老师列表
     * @return 结果
     */
    public int batchTyqcmanage(List<Tyqcmanage> tyqcmanageList);
    

    /**
     * 通过器材管理主键删除管理器材老师信息
     * 
     * @param id 器材管理ID
     * @return 结果
     */
    public int deleteTyqcmanageById(Long id);

    int addOne(Integer id);
}
