package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Tyqcg;
import com.ruoyi.system.domain.Tyqc;

/**
 * 管理器材老师Mapper接口
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
public interface TyqcgMapper 
{
    /**
     * 查询管理器材老师
     * 
     * @param id 管理器材老师主键
     * @return 管理器材老师
     */
    public Tyqcg selectTyqcgById(Long id);

    /**
     * 查询管理器材老师列表
     * 
     * @param tyqcg 管理器材老师
     * @return 管理器材老师集合
     */
    public List<Tyqcg> selectTyqcgList(Tyqcg tyqcg);

    /**
     * 新增管理器材老师
     * 
     * @param tyqcg 管理器材老师
     * @return 结果
     */
    public int insertTyqcg(Tyqcg tyqcg);

    /**
     * 修改管理器材老师
     * 
     * @param tyqcg 管理器材老师
     * @return 结果
     */
    public int updateTyqcg(Tyqcg tyqcg);

    /**
     * 删除管理器材老师
     * 
     * @param id 管理器材老师主键
     * @return 结果
     */
    public int deleteTyqcgById(Long id);

    /**
     * 批量删除管理器材老师
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTyqcgByIds(String[] ids);

    /**
     * 批量删除器材管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTyqcByIds(String[] ids);
    
    /**
     * 批量新增器材管理
     * 
     * @param tyqcList 器材管理列表
     * @return 结果
     */
    public int batchTyqc(List<Tyqc> tyqcList);
    

    /**
     * 通过管理器材老师主键删除器材管理信息
     * 
     * @param id 管理器材老师ID
     * @return 结果
     */
    public int deleteTyqcById(Long id);
}
