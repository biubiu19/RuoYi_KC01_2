package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Tyqcmanage;

/**
 * 管理器材老师Mapper接口
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
public interface TyqcmanageMapper 
{
    /**
     * 查询管理器材老师
     * 
     * @param id 管理器材老师主键
     * @return 管理器材老师
     */
    public Tyqcmanage selectTyqcmanageById(Long id);

    /**
     * 查询管理器材老师列表
     * 
     * @param tyqcmanage 管理器材老师
     * @return 管理器材老师集合
     */
    public List<Tyqcmanage> selectTyqcmanageList(Tyqcmanage tyqcmanage);

    /**
     * 新增管理器材老师
     * 
     * @param tyqcmanage 管理器材老师
     * @return 结果
     */
    public int insertTyqcmanage(Tyqcmanage tyqcmanage);

    /**
     * 修改管理器材老师
     * 
     * @param tyqcmanage 管理器材老师
     * @return 结果
     */
    public int updateTyqcmanage(Tyqcmanage tyqcmanage);

    /**
     * 删除管理器材老师
     * 
     * @param id 管理器材老师主键
     * @return 结果
     */
    public int deleteTyqcmanageById(Long id);

    /**
     * 批量删除管理器材老师
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTyqcmanageByIds(String[] ids);
}
