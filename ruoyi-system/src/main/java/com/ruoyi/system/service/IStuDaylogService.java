package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.StuDaylog;

/**
 * 日志Service接口
 * 
 * @author ruoyi
 * @date 2024-02-05
 */
public interface IStuDaylogService 
{
    /**
     * 查询日志
     * 
     * @param logId 日志主键
     * @return 日志
     */
    public StuDaylog selectStuDaylogByLogId(Long logId);

    /**
     * 查询日志列表
     * 
     * @param stuDaylog 日志
     * @return 日志集合
     */
    public List<StuDaylog> selectStuDaylogList(StuDaylog stuDaylog);

    /**
     * 新增日志
     * 
     * @param stuDaylog 日志
     * @return 结果
     */
    public int insertStuDaylog(StuDaylog stuDaylog);

    /**
     * 修改日志
     * 
     * @param stuDaylog 日志
     * @return 结果
     */
    public int updateStuDaylog(StuDaylog stuDaylog);

    /**
     * 批量删除日志
     * 
     * @param logIds 需要删除的日志主键集合
     * @return 结果
     */
    public int deleteStuDaylogByLogIds(String logIds);

    /**
     * 删除日志信息
     * 
     * @param logId 日志主键
     * @return 结果
     */
    public int deleteStuDaylogByLogId(Long logId);
}
