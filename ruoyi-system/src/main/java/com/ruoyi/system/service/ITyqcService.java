package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Tyqc;

/**
 * 器材管理Service接口
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
public interface ITyqcService 
{
    /**
     * 查询器材管理
     * 
     * @param id 器材管理主键
     * @return 器材管理
     */
    public Tyqc selectTyqcById(Long id);

    /**
     * 查询器材管理列表
     * 
     * @param tyqc 器材管理
     * @return 器材管理集合
     */
    public List<Tyqc> selectTyqcList(Tyqc tyqc);

    /**
     * 新增器材管理
     * 
     * @param tyqc 器材管理
     * @return 结果
     */
    public int insertTyqc(Tyqc tyqc);

    /**
     * 修改器材管理
     * 
     * @param tyqc 器材管理
     * @return 结果
     */
    public int updateTyqc(Tyqc tyqc);

    /**
     * 批量删除器材管理
     * 
     * @param ids 需要删除的器材管理主键集合
     * @return 结果
     */
    public int deleteTyqcByIds(String ids);

    /**
     * 删除器材管理信息
     * 
     * @param id 器材管理主键
     * @return 结果
     */
    public int deleteTyqcById(Long id);

    int addOne(Integer id);
}
