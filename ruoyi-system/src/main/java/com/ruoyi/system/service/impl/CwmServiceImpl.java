package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CwmMapper;
import com.ruoyi.system.domain.Cwm;
import com.ruoyi.system.service.ICwmService;
import com.ruoyi.common.core.text.Convert;

/**
 * 宠物猫信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-02-21
 */
@Service
public class CwmServiceImpl implements ICwmService 
{
    @Autowired
    private CwmMapper cwmMapper;

    /**
     * 查询宠物猫信息
     * 
     * @param dogId 宠物猫信息主键
     * @return 宠物猫信息
     */
    @Override
    public Cwm selectCwmByDogId(Long dogId)
    {
        return cwmMapper.selectCwmByDogId(dogId);
    }

    /**
     * 查询宠物猫信息列表
     * 
     * @param cwm 宠物猫信息
     * @return 宠物猫信息
     */
    @Override
    public List<Cwm> selectCwmList(Cwm cwm)
    {
        return cwmMapper.selectCwmList(cwm);
    }

    /**
     * 新增宠物猫信息
     * 
     * @param cwm 宠物猫信息
     * @return 结果
     */
    @Override
    public int insertCwm(Cwm cwm)
    {
        return cwmMapper.insertCwm(cwm);
    }

    /**
     * 修改宠物猫信息
     * 
     * @param cwm 宠物猫信息
     * @return 结果
     */
    @Override
    public int updateCwm(Cwm cwm)
    {
        return cwmMapper.updateCwm(cwm);
    }

    /**
     * 批量删除宠物猫信息
     * 
     * @param dogIds 需要删除的宠物猫信息主键
     * @return 结果
     */
    @Override
    public int deleteCwmByDogIds(String dogIds)
    {
        return cwmMapper.deleteCwmByDogIds(Convert.toStrArray(dogIds));
    }

    /**
     * 删除宠物猫信息信息
     * 
     * @param dogId 宠物猫信息主键
     * @return 结果
     */
    @Override
    public int deleteCwmByDogId(Long dogId)
    {
        return cwmMapper.deleteCwmByDogId(dogId);
    }
}
