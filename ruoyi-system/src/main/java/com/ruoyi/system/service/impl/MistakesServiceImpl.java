package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MistakesMapper;
import com.ruoyi.system.domain.Mistakes;
import com.ruoyi.system.service.IMistakesService;
import com.ruoyi.common.core.text.Convert;

/**
 * 错题管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-11
 */
@Service
public class MistakesServiceImpl implements IMistakesService 
{
    @Autowired
    private MistakesMapper mistakesMapper;

    /**
     * 查询错题管理
     * 
     * @param id 错题管理主键
     * @return 错题管理
     */
    @Override
    public Mistakes selectMistakesById(Long id)
    {
        return mistakesMapper.selectMistakesById(id);
    }

    /**
     * 查询错题管理列表
     * 
     * @param mistakes 错题管理
     * @return 错题管理
     */
    @Override
    public List<Mistakes> selectMistakesList(Mistakes mistakes)
    {
        return mistakesMapper.selectMistakesList(mistakes);
    }

    /**
     * 新增错题管理
     * 
     * @param mistakes 错题管理
     * @return 结果
     */
    @Override
    public int insertMistakes(Mistakes mistakes)
    {
        return mistakesMapper.insertMistakes(mistakes);
    }

    /**
     * 修改错题管理
     * 
     * @param mistakes 错题管理
     * @return 结果
     */
    @Override
    public int updateMistakes(Mistakes mistakes)
    {
        return mistakesMapper.updateMistakes(mistakes);
    }

    /**
     * 批量删除错题管理
     * 
     * @param ids 需要删除的错题管理主键
     * @return 结果
     */
    @Override
    public int deleteMistakesByIds(String ids)
    {
        return mistakesMapper.deleteMistakesByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除错题管理信息
     * 
     * @param id 错题管理主键
     * @return 结果
     */
    @Override
    public int deleteMistakesById(Long id)
    {
        return mistakesMapper.deleteMistakesById(id);
    }
}
