package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StuDaylogMapper;
import com.ruoyi.system.domain.StuDaylog;
import com.ruoyi.system.service.IStuDaylogService;
import com.ruoyi.common.core.text.Convert;

/**
 * 日志Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-02-05
 */
@Service
public class StuDaylogServiceImpl implements IStuDaylogService 
{
    @Autowired
    private StuDaylogMapper stuDaylogMapper;

    /**
     * 查询日志
     * 
     * @param logId 日志主键
     * @return 日志
     */
    @Override
    public StuDaylog selectStuDaylogByLogId(Long logId)
    {
        return stuDaylogMapper.selectStuDaylogByLogId(logId);
    }

    /**
     * 查询日志列表
     * 
     * @param stuDaylog 日志
     * @return 日志
     */
    @Override
    public List<StuDaylog> selectStuDaylogList(StuDaylog stuDaylog)
    {
        return stuDaylogMapper.selectStuDaylogList(stuDaylog);
    }

    /**
     * 新增日志
     * 
     * @param stuDaylog 日志
     * @return 结果
     */
    @Override
    public int insertStuDaylog(StuDaylog stuDaylog)
    {
        return stuDaylogMapper.insertStuDaylog(stuDaylog);
    }

    /**
     * 修改日志
     * 
     * @param stuDaylog 日志
     * @return 结果
     */
    @Override
    public int updateStuDaylog(StuDaylog stuDaylog)
    {
        return stuDaylogMapper.updateStuDaylog(stuDaylog);
    }

    /**
     * 批量删除日志
     * 
     * @param logIds 需要删除的日志主键
     * @return 结果
     */
    @Override
    public int deleteStuDaylogByLogIds(String logIds)
    {
        return stuDaylogMapper.deleteStuDaylogByLogIds(Convert.toStrArray(logIds));
    }

    /**
     * 删除日志信息
     * 
     * @param logId 日志主键
     * @return 结果
     */
    @Override
    public int deleteStuDaylogByLogId(Long logId)
    {
        return stuDaylogMapper.deleteStuDaylogByLogId(logId);
    }
}
