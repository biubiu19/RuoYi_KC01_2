package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.system.domain.Tyqcmanage;
import com.ruoyi.system.mapper.TyqcMapper;
import com.ruoyi.system.domain.Tyqc;
import com.ruoyi.system.service.ITyqcService;
import com.ruoyi.common.core.text.Convert;

/**
 * 器材管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
@Service
public class TyqcServiceImpl implements ITyqcService 
{
    @Autowired
    private TyqcMapper tyqcMapper;

    /**
     * 查询器材管理
     * 
     * @param id 器材管理主键
     * @return 器材管理
     */
    @Override
    public Tyqc selectTyqcById(Long id)
    {
        return tyqcMapper.selectTyqcById(id);
    }

    /**
     * 查询器材管理列表
     * 
     * @param tyqc 器材管理
     * @return 器材管理
     */
    @Override
    public List<Tyqc> selectTyqcList(Tyqc tyqc)
    {
        return tyqcMapper.selectTyqcList(tyqc);
    }

    /**
     * 新增器材管理
     * 
     * @param tyqc 器材管理
     * @return 结果
     */
    @Transactional
    @Override
    public int insertTyqc(Tyqc tyqc)
    {
        int rows = tyqcMapper.insertTyqc(tyqc);
        insertTyqcmanage(tyqc);
        return rows;
    }

    /**
     * 修改器材管理
     * 
     * @param tyqc 器材管理
     * @return 结果
     */
    @Transactional
    @Override
    public int updateTyqc(Tyqc tyqc)
    {
        tyqcMapper.deleteTyqcmanageById(tyqc.getId());
        insertTyqcmanage(tyqc);
        return tyqcMapper.updateTyqc(tyqc);
    }

    /**
     * 批量删除器材管理
     * 
     * @param ids 需要删除的器材管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteTyqcByIds(String ids)
    {
        tyqcMapper.deleteTyqcmanageByIds(Convert.toStrArray(ids));
        return tyqcMapper.deleteTyqcByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除器材管理信息
     * 
     * @param id 器材管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteTyqcById(Long id)
    {
        tyqcMapper.deleteTyqcmanageById(id);
        return tyqcMapper.deleteTyqcById(id);
    }

    @Override
    public int addOne(Integer id) {
        return tyqcMapper.addOne(id);
    }

    /**
     * 新增管理器材老师信息
     * 
     * @param tyqc 器材管理对象
     */
    public void insertTyqcmanage(Tyqc tyqc)
    {
        List<Tyqcmanage> tyqcmanageList = tyqc.getTyqcmanageList();
        Long id = tyqc.getId();
        if (StringUtils.isNotNull(tyqcmanageList))
        {
            List<Tyqcmanage> list = new ArrayList<Tyqcmanage>();
            for (Tyqcmanage tyqcmanage : tyqcmanageList)
            {
                tyqcmanage.setId(id);
                list.add(tyqcmanage);
            }
            if (list.size() > 0)
            {
                tyqcMapper.batchTyqcmanage(list);
            }
        }
    }
}
