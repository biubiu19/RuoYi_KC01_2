package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TyqcmanageMapper;
import com.ruoyi.system.domain.Tyqcmanage;
import com.ruoyi.system.service.ITyqcmanageService;
import com.ruoyi.common.core.text.Convert;

/**
 * 管理器材老师Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
@Service
public class TyqcmanageServiceImpl implements ITyqcmanageService 
{
    @Autowired
    private TyqcmanageMapper tyqcmanageMapper;

    /**
     * 查询管理器材老师
     * 
     * @param id 管理器材老师主键
     * @return 管理器材老师
     */
    @Override
    public Tyqcmanage selectTyqcmanageById(Long id)
    {
        return tyqcmanageMapper.selectTyqcmanageById(id);
    }

    /**
     * 查询管理器材老师列表
     * 
     * @param tyqcmanage 管理器材老师
     * @return 管理器材老师
     */
    @Override
    public List<Tyqcmanage> selectTyqcmanageList(Tyqcmanage tyqcmanage)
    {
        return tyqcmanageMapper.selectTyqcmanageList(tyqcmanage);
    }

    /**
     * 新增管理器材老师
     * 
     * @param tyqcmanage 管理器材老师
     * @return 结果
     */
    @Override
    public int insertTyqcmanage(Tyqcmanage tyqcmanage)
    {
        return tyqcmanageMapper.insertTyqcmanage(tyqcmanage);
    }

    /**
     * 修改管理器材老师
     * 
     * @param tyqcmanage 管理器材老师
     * @return 结果
     */
    @Override
    public int updateTyqcmanage(Tyqcmanage tyqcmanage)
    {
        return tyqcmanageMapper.updateTyqcmanage(tyqcmanage);
    }

    /**
     * 批量删除管理器材老师
     * 
     * @param ids 需要删除的管理器材老师主键
     * @return 结果
     */
    @Override
    public int deleteTyqcmanageByIds(String ids)
    {
        return tyqcmanageMapper.deleteTyqcmanageByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除管理器材老师信息
     * 
     * @param id 管理器材老师主键
     * @return 结果
     */
    @Override
    public int deleteTyqcmanageById(Long id)
    {
        return tyqcmanageMapper.deleteTyqcmanageById(id);
    }
}
